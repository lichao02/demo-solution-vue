<<<<<<< HEAD
# 项目介绍
本项目采用VUE + ES6实现，其中jwps.es6.js是由WPS开发组特别提供，本项目对该js做了部分调整

## 本次更新
1. 增加新建模版入口，快速新建word，excel，ppt文件
2. 增加上传、删除等文件管理入口
3. 调整目录结构，调整vueconfig

## 特别注意
1. 次前端工程必须配合后台部分使用
2. main.js 中的axios.defaults.baseURL请自行更换，必须和[wps开放平台](https://solution.wps.cn)上的回调URL一致

### 演示地址
[http://139.159.254.199/#/dbFile ](http://139.159.254.199/#/dbFile )

## 后台java代码地址
[https://gitee.com/lichao02/Public-DemoSolution--New](https://gitee.com/lichao02/Public-DemoSolution--New)

## 安装依赖
```
yarn install
```

### 开发运行
```
yarn serve
```

### 生产打包
```
yarn build
```
=======
# DemoSolution-vue

#### 介绍
vue项目

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
>>>>>>> f18f55677a29eea17bc4b55f0ee7b970519af249
